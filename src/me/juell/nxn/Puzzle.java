package me.juell.nxn;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;

public class Puzzle {
	
	private State startingState;
	private PriorityQueue<State> statesToVisit;
	private Set<State> statesVisited;
	
	public Puzzle(State startingState) {
		this.startingState = startingState;
		statesToVisit = new PriorityQueue<>(10, new State.FCostComparator());
		statesVisited = new HashSet<State>();
	}

	public static void main(String[] args) {
		Puzzle puzzle = readPuzzle();
		//System.out.println(puzzle.startingState.toString());
		puzzle.solve();
	}

	private static Puzzle readPuzzle() {
		Scanner sc = null;
		try {
			sc = new Scanner(new File("cases/Obl2-Opg2-Test1.txt"));
			int rowWithEmpty = -1; 
			int colWithEmpty = -1;
			int n = sc.nextInt();
			byte [][] board = new byte[n][n];
			for(int i = 0 ; i < n; i++){
				for(int j = 0; j < n; j++){
					board[i][j] = sc.nextByte(10);
					if(board[i][j] == 0){
						rowWithEmpty = i;
						colWithEmpty = j;
					}
				}
			}
			Puzzle puzzle = new Puzzle(new State(board, n, rowWithEmpty, colWithEmpty));
			return puzzle;
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}finally{
			if(sc != null){
				sc.close();
			}
		}
		
	}
	
	public State solve(){
		statesToVisit.add(startingState);
		State currentState = null;
		while(!statesToVisit.isEmpty()){
			currentState = statesToVisit.poll();
			statesVisited.add(currentState);
//			System.out.println(currentState);
//			System.out.println("f: " + currentState.fCost() + " g: " + currentState.gCost() + " h:" + currentState.hCost());
//			System.out.println("stv: " + statesToVisit.size() + ", sv'd: " + statesVisited.size());
//			System.out.println();
//			System.out.println();
			if(currentState.isSolution()){
				printSolution(currentState);
				return currentState;
			}
			for(Move move : Move.values()){
				State candidateMove = currentState.move(move);
				if(candidateMove != null && !statesVisited.contains(candidateMove) && !statesToVisit.contains(candidateMove)){
					statesToVisit.add(candidateMove);
				}
			}
		}
		return null;
	}
	
	public void printSolution (State solutionState){
		System.out.println(statesVisited.size());
		StringBuilder reverseSolution = new StringBuilder();
		State state = solutionState;
		while (state.previousMove != null){
			reverseSolution.append(state.previousMove.toString());
			state = state.parent;
		}
		System.out.println(reverseSolution.reverse().toString());
		System.out.println(statesToVisit.size() + statesVisited.size());
	}

}
