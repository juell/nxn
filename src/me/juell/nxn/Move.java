package me.juell.nxn;

public enum Move {
	UP("O"), DOWN("N"), RIGHT("H"), LEFT("V");
	
	private String output;
	
	private Move(String output){
		this.output = output;
	}
	
	@Override
	public String toString() {
		return output;
	}
	
}
