package me.juell.nxn;

import java.util.Arrays;
import java.util.Comparator;


public class State {
	byte [][] board; 
	int n;
	int rowWithEmpty;
	int colWithEmpty;
	State parent;
	int previousMoves;
	Move previousMove;
	private int manhattanDistSum = -1; // 
	
	public State(byte[][] board, int n, int rowWithEmpty, int colWithEmpty) {
		this.board = board;
		this.n = n;
		this.rowWithEmpty = rowWithEmpty;
		this.colWithEmpty = colWithEmpty;
	}

	private State createMoveState(Move move, int switchRow, int switchCol){
		byte[][] newBoard = cloneBoard();
		newBoard[rowWithEmpty][colWithEmpty] = newBoard[switchRow][switchCol];
		newBoard[switchRow][switchCol] = 0;
		State state = new State(newBoard, n, switchRow, switchCol);
		state.parent = this;
		state.previousMoves = this.previousMoves + 1;
		state.previousMove = move;
		return state;
	}

	public State move(Move move){
		
		int switchRow = rowWithEmpty;
		int switchCol = colWithEmpty;
		
		switch (move) {
		case UP:
			switchRow--;
			break;
		case DOWN:
			switchRow++;
			break;
		case RIGHT:
			switchCol++;
			break;
		case LEFT: 
			switchCol--;
			break;
		default:
		}

		if(switchCol < 0 || switchCol >= n || switchRow < 0 || switchRow >= n ){
			return null;
		}		
		
		return createMoveState(move, switchRow, switchCol);
	}
	
	public int hCost(){
		if(manhattanDistSum < 0){
			manhattanDistSum = 0;
			for(int i = 0; i < n; i++){
				for(int j = 0; j < n; j++){
					//0 at top left, n-1 at top right, n^2-1 at bottom right
					int targetPos = board[i][j];
					//For some reason the 0 goes in last rather than the first square - hack fix
					targetPos = (targetPos == 0 ? n*n-1 : targetPos -1); 
					manhattanDistSum += Math.abs(i - (targetPos / n)); 
					manhattanDistSum += Math.abs(j - (targetPos % n)); 
				}
			}
		}
		return manhattanDistSum;
	}
	
	public int gCost(){
		return previousMoves;
	}
	
	public int fCost(){
		return gCost() + hCost();
	}
	
	private byte[][] cloneBoard(){
		byte [][] clone = new byte[n][n];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++){
				clone[i][j] = board[i][j];
			}
		}
		return clone;
	}

	public boolean isSolution() {
		return hCost() == 0;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof State)){
			return false;
		}
		return Arrays.deepEquals(board, ((State)obj).board);
	}
	
	@Override
	public int hashCode() {
		return Arrays.deepHashCode(board);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i< n; i++){
			for (int j = 0; j < n; j++){
				sb.append(String.format(" %2s", board[i][j]));
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public static class FCostComparator implements Comparator<State>{

		@Override
		public int compare(State o1, State o2) {
			return o1.fCost() - o2.fCost();
		}
		
	}
}
